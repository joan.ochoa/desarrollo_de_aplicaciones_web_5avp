**Desarrolla aplicaciones web con conexion a base de datos**
*Alumno: Ochoa Iribe Joan Elizandro*
*Grupo y grado: 5AVP*
- Practica 1 - 02/09/2022 - Practica de ejemplo 
Commit: eca2bac743a1ec2711fd00e255f40fdf8a81bb46
Archivo: https://gitlab.com/joan.ochoa/desarrollo_de_aplicaciones_web_5avp/-/blob/main/parcial1/practica_ejemplo.html 

- Practica 2 - 09/09/2022 - Practica de JavaScript
commit: b9abc42d71f044edd5c470d3ac853f836a972db1
Archivo: https://gitlab.com/joan.ochoa/desarrollo_de_aplicaciones_web_5avp/-/blob/main/parcial1/PracticaJavaScript.html

- Practica 3 - 15/09/2022 - Practica web con base de datos
commit: ff9d91964824d1e1afba8fb9b07f9d2d50e632f8

- Practica 4 - 19/09/2022 - Practica WEB con base de datos - Vista de consulta de datos
commit: 5e19cc1ce97169804f95464eda9ad8b7c770c06e

- Practica 5 - 22/09/2022 - Practica WEB con base de datos - registra base de datos
commit: c3d4527445bbbc29b6c8058e2ed21fdf84eed0e3


- Practica 6 - 26/09/2022 - Practica WEB  con base de datos- conexion
commit: 19f30772dde7ce6ae6579c6c816f04921be3d5f2
